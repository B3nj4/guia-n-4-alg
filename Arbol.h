#ifndef ARBOL_H
#define ARBOL_H

//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
using namespace std;

// Clase de nodo 
typedef struct _Nodo {
    int numero;
    struct _Nodo *izq;
    struct _Nodo *der;
    struct _Nodo *padre;
} Nodo;

// Clase de grafo
class Grafo {
    private:
    
    public:
        Grafo (Nodo *apnodo) {
            ofstream archivo;
            
            /* abre archivo */
            archivo.open ("grafo.txt");

            archivo << "digraph G {" << endl;
            archivo << "node [style=filled fillcolor=yellow];" << endl;
                
            recorrer(apnodo, archivo);

            archivo << "}" << endl;

            /* cierra archivo */
            archivo.close();
                    
            /* genera el grafo */
            system("dot -Tpng -ografo.png grafo.txt &");
            
            /* visualiza el grafo */
            system("eog grafo.png &");
        }
        
        /*
        * recorre en árbol en preorden y agrega datos al archivo.
        */
        void recorrer(Nodo *apnodo, ofstream &archivo) { 
            if (apnodo != NULL) {
                if (apnodo->izq != NULL) {
                    archivo << "\n" << apnodo->numero  << "->" << apnodo->izq->numero << ";";
                } else {      
                    archivo << "\n" << '"' << apnodo->numero << "i" << '"' << " [shape=point];";
                    archivo << "\n" << apnodo->numero << "->" << '"' << apnodo->numero << "i" << '"';
                }
                    
                if (apnodo->der != NULL) { 
                    archivo << apnodo->numero << "->" << apnodo->der->numero << ";";
                } else {
                    archivo << "\n" << '"' << apnodo->numero << "d" << '"' << " [shape=point];";
                    archivo << "\n"<< apnodo->numero << "->" << '"' << apnodo->numero << "d" << '"' ;
                }

                recorrer(apnodo->izq, archivo);
                recorrer(apnodo->der, archivo); 
            }
            //return;
        }
};

// CLase del arbol
class Arbol {

    public:
        Arbol();
        
        // Insercion
        void insertar(Nodo *&apnodo, int info, Nodo *padre);
        
        // Eliminacion
        Nodo *minimo(Nodo *apnodo);
        void reemplazar(Nodo *apnodo,Nodo *nuevoNodo);
        void destruirNodo(Nodo *nodo);
        void eliminarNodo(Nodo *nodoEliminar);
        void eliminar(Nodo *apnodo, int info);
               
        // Mostrar
        void mostrar_preorden(Nodo *apnodo);
        void mostrar_inorden(Nodo *apnodo);
        void mostrar_posorden(Nodo *apnodo);

        // Modificacion
        bool busqueda(Nodo *apnodo, int n);

};
#endif
