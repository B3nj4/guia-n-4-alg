//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
using namespace std;

#include "Arbol.h"

// Menu
int menu(){
    string opc;

    cout << "\n";
    cout << "-------------------------------" << endl;
    cout << "Insertar numero             [1]" << endl;
    cout << "Eliminar numero             [2]" << endl;
    cout << "Modificar elemento          [3]" << endl;
    cout << "Mostrar contenido           [4]" << endl;
    cout << "Generar grafo               [5]" << endl;
    cout << "Salir                       [0]" << endl;
    cout << "-------------------------------" << endl;
    cout << "Opcion: ";
    cin >> opc;

  return stoi(opc);
}

// Funcion principal del programa
int main(void) {

    // Creacion de Objetos
    Arbol *arbol = new Arbol();
    Nodo *aux = NULL;

    //Variables
    int opc = 0;
    int line;

    system("clear");

    //Interactuar con el menu
    do {
        opc = menu();

        switch (opc) {
            //Agregar nombre y mostrar informacion
            case 1:
                cout << "Ingrese numero que desee insertar: ";
                cin >> line;
                arbol->insertar(aux, line, NULL);
                break;

            //Ingresar un nombre y si hay coincidencia eliminarlo
            case 2:
                cout << "Ingrese numero que desee eliminar: ";
                cin >> line;
                arbol->eliminar(aux, line);
                break;

            //Ingresar numero y si hay coincidencia lo modifica por teclado
            case 3:    
                cout << "Ingrese numero que desee modificar: ";
                cin >> line;
                if(arbol->busqueda(aux,line) == true){
                    //cout << "Elemento " << line << " encontrado" << endl;
                    arbol->eliminar(aux, line);
                    cout << endl;
                    cout << "Ingrese dato por el que desea modificar: ";
                    cin >> line;
                    arbol->insertar(aux, line, NULL);
                }
                else{
                    cout << "Elemento no encontrado ";
                }
                cout << endl;                
                break;

            // Se muestran los tres ordenes del arbol
            case 4:
                cout << "\nElementos en preorden: ";
                arbol->mostrar_preorden(aux);
                cout << "\nElementos en inorden: ";
                arbol->mostrar_inorden(aux);
                cout << "\nElementos en preorden: ";
                arbol->mostrar_posorden(aux);
                cout << endl;
                break;

            // Genera el grafo
            case 5:
                cout << endl;
                Grafo *g = new Grafo(aux);
                cout << endl;
                break;
                
        }
    } while (opc != 0); // Sale del programa

    //Se eliminan espacios de memoria
    delete arbol;
    delete aux;
    
    return 0;
}
