#include <fstream>
#include <iostream>
using namespace std;

//Librerias
#include "Arbol.h"

Arbol::Arbol() {}

// Crear nodo
Nodo *crear_Nodo(int info, Nodo *padre){
    Nodo *aux = new Nodo();
    aux->der = NULL;
    aux->izq = NULL;
    aux->numero = info;
    aux->padre = padre;
    return aux;
}

// Insertar elememento
void Arbol::insertar(Nodo *&apnodo, int info, Nodo *padre) {    
    // En caso de que apnodo este vacio
    if(apnodo == NULL){
        Nodo *otro = crear_Nodo(info,padre);
        apnodo = otro;
    }
    // Se insertan los nodos y valores correspondientes
    else{
        int valor_raiz = apnodo->numero;
        if(info < valor_raiz){
            insertar(apnodo->izq, info, apnodo);
            cout << "Se ha ingresado elemento" << endl;
        }
        else{
            if(info > valor_raiz){
            insertar(apnodo->der, info, apnodo);
            cout << "Se ha ingresado elemento" << endl;
            }
        }
    }
}




// Encontrar el valor minimo 
Nodo *Arbol::minimo(Nodo *apnodo){
    if(apnodo == NULL){  // Si arbol esta vacio no retorna nada
        return NULL;
    }
    if(apnodo->izq){
        return minimo(apnodo->izq); // Buscar todas las partes izquierdas 
    }
    else{ // Sin nodos izquierdos retorne el arbol
        return apnodo;
    }
}

// Se reemplaza elementos para la funcion eliminar
void Arbol::reemplazar(Nodo *apnodo, Nodo *nuevoNodo){
    // Ver si nodo tiene padre
    if(apnodo->padre){ // SI no es raiz
        //modificar el hijo de padre
        if(apnodo->padre->izq != NULL){
            if(apnodo->numero == apnodo->padre->izq->numero){
                apnodo->padre->izq = nuevoNodo;
            }
        }
        
        if(apnodo->padre->der != NULL){
            if(apnodo->numero == apnodo->padre->der->numero){
                apnodo->padre->der = nuevoNodo;
            }
        }
    }
    if(nuevoNodo){
        //Asignar nuevo padre al elemento modificado
        nuevoNodo->padre = apnodo->padre;
    }
}

// Borrar nodo
void Arbol::destruirNodo(Nodo *nodo){
    nodo->izq = NULL;
    nodo->der = NULL;
    delete nodo;
}

// "ELiminar" nodo eemplazando el nodo (sucesores del nodo a borrar) menor por el nodo a eliminar
void Arbol::eliminarNodo(Nodo *nodoEliminar){
    // ELiminar nodo con dos elementos
    if(nodoEliminar->izq && nodoEliminar->der){ // Ver si tiene dos hijos
        Nodo *menor = minimo(nodoEliminar->der); // Como minimo() ve el lado izquierdo, se crea nodo para reemplazar el lado derecho 
        nodoEliminar->numero = menor->numero; // se reemplaza el nodo eliminado por el nodo menor
        eliminarNodo(menor);
    }
    
    // ELiminar nodo con un elemento en la izq
    else if(nodoEliminar->izq){
        reemplazar(nodoEliminar, nodoEliminar->izq);
        cout << "Se ha eliminado un elemento";
        destruirNodo(nodoEliminar);
    }

    // ELiminar nodo con hijo derecho
    else if(nodoEliminar->der){
        reemplazar(nodoEliminar, nodoEliminar->der);
        cout << "Se ha eliminado un elemento";
        destruirNodo(nodoEliminar);
    }

    // Hoja
    else{
        reemplazar(nodoEliminar, NULL);
        cout << "Se ha eliminado un elemento";
        destruirNodo(nodoEliminar);
    }
}

// Eliminar elemento
void Arbol::eliminar(Nodo *apnodo, int info){
    
    if(apnodo == NULL){ // SI el arbol esta vacio
        return; // NO hace nada
    }
    else if(info < apnodo->numero){
        eliminar(apnodo->izq, info);  // Busca por la izq
    }
    else if(info > apnodo->numero){
        eliminar(apnodo->der, info); // Busca por la derecha
    }
    else{
        eliminarNodo(apnodo);
    }
}




//Mostrar preorden
void Arbol::mostrar_preorden(Nodo *apnodo){
    // Verificar si esta vacio para salir de esta funcion
    if(apnodo == NULL){
        return;
    }
    // IMpresion de los elementos con un orden determinado
    else{
        cout << apnodo->numero << "   ";
        mostrar_preorden(apnodo->izq);
        mostrar_preorden(apnodo->der);
    }

}

//Mostrar inorden
void Arbol::mostrar_inorden(Nodo *apnodo){
    // Verificar si esta vacio para salir de esta funcion
    if(apnodo == NULL){
        return;
    }
    // IMpresion de los elementos con un orden determinado
    else{
        mostrar_inorden(apnodo->izq);
        cout << apnodo->numero << "   ";
        mostrar_inorden(apnodo->der);
    }
    
}

//Mostrar posorden
void Arbol::mostrar_posorden(Nodo *apnodo){
    // Verificar si esta vacio para salir de esta funcion
    if(apnodo == NULL){
        return;
    }
    // IMpresion de los elementos con un orden determinado
    else{
        mostrar_posorden(apnodo->izq);
        mostrar_posorden(apnodo->der);
        cout << apnodo->numero << "   ";   
    }
    
}

// Buscar elemento
bool Arbol::busqueda(Nodo *apnodo, int n){
    // Si el arbol esta vacio no hay elementos true
    if(apnodo == NULL){
        return false;
    }
    // si se encuentra el numero n, retorna true
    else if(apnodo->numero == n){
        return true;
    }
    // buscar por la der
    else if(n < apnodo->numero){
        return busqueda(apnodo->izq, n);
    }
    // buscar por la izq
    else{
        return busqueda(apnodo->der, n);
    }
}
